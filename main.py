import cv2
import os
from datetime import datetime


def ensure_dir(file_path):
	if not os.path.exists(file_path):
		os.makedirs(file_path)


def delete_extra_photos(directory, max_photos=100):
	photos = [os.path.join(directory, photo) for photo in os.listdir(directory)]
	while len(photos) > max_photos:
		oldest_photo = min(photos, key=os.path.getctime)
		os.remove(oldest_photo)
		photos.remove(oldest_photo)


def detect_motion_and_capture(save_directory="captured_photos"):
	ensure_dir(save_directory)  # Ensure the save directory exists
	camera = cv2.VideoCapture(0)  # Use the default camera
	ret, frame1 = camera.read()
	ret, frame2 = camera.read()

	while True:
		diff = cv2.absdiff(frame1, frame2)
		gray = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)
		blur = cv2.GaussianBlur(gray, (5, 5), 0)
		_, thresh = cv2.threshold(blur, 20, 255, cv2.THRESH_BINARY)
		dilated = cv2.dilate(thresh, None, iterations=3)
		contours, _ = cv2.findContours(dilated, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

		for contour in contours:
			if cv2.contourArea(contour) < 5000:
				continue
			# Motion detected
			(x, y, w, h) = cv2.boundingRect(contour)
			cv2.rectangle(frame1, (x, y), (x + w, y + h), (0, 255, 0), 2)
			snapshot_filename = os.path.join(save_directory, f"motion_{datetime.now().strftime('%Y%m%d_%H%M%S')}.jpg")
			cv2.imwrite(snapshot_filename, frame1)
			delete_extra_photos(save_directory)  # Check and delete photos if more than 30

		cv2.imshow("feed", frame1)
		frame1 = frame2
		ret, frame2 = camera.read()

		if cv2.waitKey(1) & 0xFF == ord('q'):
			break

	camera.release()
	cv2.destroyAllWindows()


if __name__ == "__main__":
	detect_motion_and_capture()
