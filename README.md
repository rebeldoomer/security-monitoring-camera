# Security Camera with Motion Detection and Capture

This project utilizes OpenCV to detect motion through a camera feed and captures photos of the motion. It's designed to be a simple surveillance tool that stores a defined number of the most recent motion events.

## Getting Started

Follow these instructions to get a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Python 3.x
- OpenCV library

### Installation

1. Ensure you have Python installed on your system. If not, download and install it from [python.org](https://www.python.org/downloads/).

2. Install OpenCV using pip:

`pip install opencv-python-headless`


3. Clone the repository to your local machine:

`git clone https://gitlab.com/rebeldoomer/motion-detection-and-capture.git`


4. Navigate to the cloned directory:

`cd motion-detection-and-capture`



## Usage

To start motion detection, run the script from the command line:

`python3 main.py`




Press 'q' to quit the application.

## Contributing

Contributions make the open-source community thrive. If you would like to contribute to this project, your help is very welcome. Open an issue for any bugs or feature requests or submit pull requests for your contributions.

## License

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-nc-sa/4.0/](http://creativecommons.org/licenses/by-nc-sa/4.0/)


## Project Status

This project is currently in active development. Contributions, bug reports, and feature requests are welcome.

